﻿using System.ComponentModel;

namespace Cadastro
{
    public enum MenuSelecionado
    {
        [Description("Nenhuma Opção")]
        Nenhuma,
        [Description("Cadastrar Novo Contato")]
        Cadastrar,
        [Description("Listar Contatos Cadastrados")]
        Visualizar,
        [Description("Sair")]
        Sair
    }
}
