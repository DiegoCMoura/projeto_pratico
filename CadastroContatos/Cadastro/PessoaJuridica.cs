﻿namespace Cadastro
{
    public class PessoaJuridica: Pessoa
    {
        public PessoaJuridica() {
            Contatos = new List<Contato>();
            Enderecos = new List<Endereco>();
        }

        public string CNPJ { get; set; }
        public string RazaoSocial { get; set; }
        public DateTime DataAbertura { get; set; }
    }
}
