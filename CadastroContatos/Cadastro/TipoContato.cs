﻿namespace Cadastro
{
    public enum TipoContato
    {
        Email,
        Fixo,
        Celular,
        Fax,
        Beep
    }
}
