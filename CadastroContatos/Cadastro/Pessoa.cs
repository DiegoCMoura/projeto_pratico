﻿namespace Cadastro
{
    public abstract class Pessoa
    {
        public string Nome { get; set; }

        public ICollection<Contato> Contatos { get; set; }
        public ICollection<Endereco> Enderecos { get; set; }
    }
}
