﻿namespace Cadastro
{
    public static class ChamadaCadastro
    {
        private static ICollection<Pessoa> lstPessoas;

        public static void Executar() {
            lstPessoas = new List<Pessoa>();

            Titulo();
            Loop();
        }

        private static void Titulo() {
            Console.WriteLine("Bem vindo ao Cadastro de Contatos");
            Console.WriteLine("\n");
        }

        private static void Limpar() {
            Console.Clear();
            Titulo();
        }

        private static string Menu() {
            Console.WriteLine("\nInforme o que deseja executar: ");
            Console.WriteLine("N - Nenhuma Opção");
            Console.WriteLine("C - Cadatrar Novo Contato");
            Console.WriteLine("V - Listar Contatos Cadastrados");
            Console.WriteLine("S - Sair");

            return Console.ReadLine();
        }

        private static MenuSelecionado VerificarOpcaoSelecionado(string opcao) {
            if (!string.IsNullOrEmpty(opcao) && opcao.Length == 1) {
                return opcao switch {
                    "N" => MenuSelecionado.Nenhuma,
                    "C" => MenuSelecionado.Cadastrar,
                    "V" => MenuSelecionado.Visualizar,
                    _ => MenuSelecionado.Sair,
                };
            }

            return MenuSelecionado.Nenhuma;
        }

        private static TipoPessoa VerificarTipoPessoaSelecionado(string tipo) {
            if (!string.IsNullOrEmpty(tipo) && tipo.Length == 1) {
                return tipo switch {
                    "1" => TipoPessoa.Fisica,
                    _ => TipoPessoa.Juridica,
                };
            }

            return TipoPessoa.Juridica;
        }

        private static TipoContato VerificarTipoContatoSelecionado(string tipo) {
            if (!string.IsNullOrEmpty(tipo) && tipo.Length == 1) {
                return tipo switch {
                    "1" => TipoContato.Email,
                    "2" => TipoContato.Fixo,
                    "3" => TipoContato.Celular,
                    "4" => TipoContato.Fax,
                    _ => TipoContato.Beep,
                };
            }

            return TipoContato.Beep;
        }

        private static TipoEndereco VerificarTipoEnderecoSelecionado(string tipo) {
            if (!string.IsNullOrEmpty(tipo) && tipo.Length == 1) {
                return tipo switch {
                    "1" => TipoEndereco.Comercial,
                    "2" => TipoEndereco.Entrega,
                    "3" => TipoEndereco.Cobranca,
                    _ => TipoEndereco.Nenhum,
                };
            }

            return TipoEndereco.Nenhum;
        }

        private static void Loop() {
            MenuSelecionado menuSelecionado = MenuSelecionado.Nenhuma;

            do {
                var opcao = Menu();
                menuSelecionado = VerificarOpcaoSelecionado(opcao.ToUpper());

                switch (menuSelecionado) {
                    case MenuSelecionado.Nenhuma:
                        break;
                    case MenuSelecionado.Cadastrar:
                        Cadastrar();
                        break;
                    case MenuSelecionado.Visualizar:
                        Visualizar();
                        break;
                    case MenuSelecionado.Sair:
                        break;
                    default:
                        break;
                }
            } while (menuSelecionado != MenuSelecionado.Sair);
        }

        private static void Visualizar() {
            Console.WriteLine("\nPessoas:");

            if (lstPessoas == null || lstPessoas.Count == 0) {
                Console.WriteLine($"Nenhuma pessoa cadastrada !!!\n");
            } else {
                foreach (var item in lstPessoas) {
                    if (item.GetType() == typeof(PessoaFisica)) {
                        var pessoa = (PessoaFisica)item;
                        Console.WriteLine($"Nome: {pessoa.Nome}");
                        Console.WriteLine($"CPF: {pessoa.CPF:999.999.999-99}");
                        Console.WriteLine($"RG: {pessoa.RG}");
                        Console.WriteLine($"Dt Nascimento: {pessoa.DataNascimento:dd/MM/yyyy}");

                        foreach (var contato in pessoa?.Contatos) {
                            Console.WriteLine(contato.ToString());
                        }
                        foreach (var endereco in pessoa?.Enderecos) {
                            Console.WriteLine(endereco.ToString());
                        }
                    } else {
                        var pessoa = (PessoaJuridica)item;
                        Console.WriteLine($"Nome: {pessoa.Nome}");
                        Console.WriteLine($"Razão Social: {pessoa.RazaoSocial}");
                        Console.WriteLine($"CPF: {pessoa.CNPJ:99.999.999/9999-99}");
                        Console.WriteLine($"Dt Nascimento: {pessoa.DataAbertura:dd/MM/yyyy}");

                        foreach (var contato in pessoa?.Contatos) {
                            Console.WriteLine(contato.ToString());
                        }
                        foreach (var endereco in pessoa?.Enderecos) {
                            Console.WriteLine(endereco.ToString());
                        }
                    }
                }
            }
        }

        private static void Cadastrar() {
            Console.WriteLine("Informe qual o tipo de pessoa que desejar cadastrar: ");
            Console.WriteLine("1 - Fisica");
            Console.WriteLine("2 - Juridica");
            var tipo = Console.ReadLine();
            var tipoSelecionado = VerificarTipoPessoaSelecionado(tipo.ToUpper());

            switch (tipoSelecionado) {
                case TipoPessoa.Fisica:
                    CadastrarPF();
                    break;
                default:
                    CadastrarPJ();
                    break;
            }
        }

        private static void CadastrarPF() {
            var pessoa = new PessoaFisica();
            Console.Write("Informe o numero CPF: ");
            pessoa.CPF = Console.ReadLine();
            Console.Write("Informe o numero RG: ");
            pessoa.RG = Console.ReadLine();
            Console.Write("Informe o nome: ");
            pessoa.Nome = Console.ReadLine();
            Console.Write("Informe a data de nascimento: ");
            var dataNasc = Console.ReadLine();
            if (DateTime.TryParse(dataNasc, out DateTime dateValue)) {
                pessoa.DataNascimento = dateValue;
            }

            Console.WriteLine("Deseja cadastrar algum contato? S para Sim ou N para Não");
            var cadastrarContato = Console.ReadLine();

            if (!string.IsNullOrEmpty(cadastrarContato) && (cadastrarContato.ToUpper() == "S")) {
                TipoContato tipoContato = TipoContato.Beep;

                do {
                    Console.WriteLine("Informe qual o tipo de contato que desejar cadastrar: ");
                    Console.WriteLine("1 - Email");
                    Console.WriteLine("2 - Telefone Fixo");
                    Console.WriteLine("3 - Telefone Celular");
                    Console.WriteLine("4 - Fax");
                    Console.WriteLine("5 - Sair");
                    var tipo = Console.ReadLine();
                    tipoContato = VerificarTipoContatoSelecionado(tipo);

                    switch (tipoContato) {
                        case TipoContato.Email:
                            pessoa.Contatos.Add(CadastrarContato(tipoContato));
                            break;
                        case TipoContato.Fixo:
                            pessoa.Contatos.Add(CadastrarContato(tipoContato));
                            break;
                        case TipoContato.Celular:
                            pessoa.Contatos.Add(CadastrarContato(tipoContato));
                            break;
                        case TipoContato.Fax:
                            pessoa.Contatos.Add(CadastrarContato(tipoContato));
                            break;
                        default:
                            break;
                    }

                } while (tipoContato != TipoContato.Beep);
            }

            Console.WriteLine("Deseja cadastrar algum Endereco? S para Sim ou N para Não");
            var cadastrarEndereco = Console.ReadLine();

            if (!string.IsNullOrEmpty(cadastrarEndereco) && (cadastrarEndereco.ToUpper() == "S")) {
                TipoEndereco tipoEndereco = TipoEndereco.Nenhum;

                do {
                    Console.WriteLine("Informe qual o tipo de endereco que desejar cadastrar: ");
                    Console.WriteLine("1 - Comercial");
                    Console.WriteLine("2 - Entrega");
                    Console.WriteLine("3 - Cobrança");
                    Console.WriteLine("4 - Sair");
                    var tipo = Console.ReadLine();
                    tipoEndereco = VerificarTipoEnderecoSelecionado(tipo);

                    switch (tipoEndereco) {
                        case TipoEndereco.Comercial:
                            pessoa.Enderecos.Add(CadastrarEndereco(tipoEndereco));
                            break;
                        case TipoEndereco.Entrega:
                            pessoa.Enderecos.Add(CadastrarEndereco(tipoEndereco));
                            break;
                        case TipoEndereco.Cobranca:
                            pessoa.Enderecos.Add(CadastrarEndereco(tipoEndereco));
                            break;
                        default:
                            break;
                    }

                } while (tipoEndereco != TipoEndereco.Nenhum);
            }

            lstPessoas.Add(pessoa);
        }

        private static void CadastrarPJ() {
            var pessoa = new PessoaJuridica();
            Console.Write("Informe o numero CNPJ: ");
            pessoa.CNPJ = Console.ReadLine();
            Console.Write("Informe a razão social: ");
            pessoa.RazaoSocial = Console.ReadLine();
            Console.Write("Informe o nome: ");
            pessoa.Nome = Console.ReadLine();
            Console.Write("Informe a data de abertura: ");
            var dataAbert = Console.ReadLine();
            if (DateTime.TryParse(dataAbert, out DateTime dateValue)) {
                pessoa.DataAbertura = dateValue;
            }

            Console.WriteLine("Deseja cadastrar algum contato? S para Sim ou N para Não");
            var cadastrarContato = Console.ReadLine();

            if (!string.IsNullOrEmpty(cadastrarContato) && (cadastrarContato.ToUpper() == "S")) {
                TipoContato tipoContato = TipoContato.Beep;

                do {
                    Console.WriteLine("Informe qual o tipo de contato que desejar cadastrar: ");
                    Console.WriteLine("1 - Email");
                    Console.WriteLine("2 - Telefone Fixo");
                    Console.WriteLine("3 - Telefone Celular");
                    Console.WriteLine("4 - Fax");
                    Console.WriteLine("5 - Sair");
                    var tipo = Console.ReadLine();
                    tipoContato = VerificarTipoContatoSelecionado(tipo);

                    switch (tipoContato) {
                        case TipoContato.Email:
                            pessoa.Contatos.Add(CadastrarContato(tipoContato));
                            break;
                        case TipoContato.Fixo:
                            pessoa.Contatos.Add(CadastrarContato(tipoContato));
                            break;
                        case TipoContato.Celular:
                            pessoa.Contatos.Add(CadastrarContato(tipoContato));
                            break;
                        case TipoContato.Fax:
                            pessoa.Contatos.Add(CadastrarContato(tipoContato));
                            break;
                        default:
                            break;
                    }

                } while (tipoContato != TipoContato.Beep);
            }

            Console.WriteLine("Deseja cadastrar algum Endereco? S para Sim ou N para Não");
            var cadastrarEndereco = Console.ReadLine();

            if (!string.IsNullOrEmpty(cadastrarEndereco) && (cadastrarEndereco.ToUpper() == "S")) {
                TipoEndereco tipoEndereco = TipoEndereco.Nenhum;

                do {
                    Console.WriteLine("Informe qual o tipo de endereco que desejar cadastrar: ");
                    Console.WriteLine("1 - Comercial");
                    Console.WriteLine("2 - Entrega");
                    Console.WriteLine("3 - Cobrança");
                    Console.WriteLine("4 - Sair");
                    var tipo = Console.ReadLine();
                    tipoEndereco = VerificarTipoEnderecoSelecionado(tipo);

                    switch (tipoEndereco) {
                        case TipoEndereco.Comercial:
                            pessoa.Enderecos.Add(CadastrarEndereco(tipoEndereco));
                            break;
                        case TipoEndereco.Entrega:
                            pessoa.Enderecos.Add(CadastrarEndereco(tipoEndereco));
                            break;
                        case TipoEndereco.Cobranca:
                            pessoa.Enderecos.Add(CadastrarEndereco(tipoEndereco));
                            break;
                        default:
                            break;
                    }

                } while (tipoEndereco != TipoEndereco.Nenhum);
            }

            lstPessoas.Add(pessoa);
        }

        private static Contato CadastrarContato(TipoContato tipo) {
            var contato = new Contato();
            Console.Write("Informe a informação de contato: ");
            contato.Tipo = tipo;
            contato.Dado = Console.ReadLine();
            return contato;
        }

        private static Endereco CadastrarEndereco(TipoEndereco tipo) {
            var endereco = new Endereco();
            endereco.Tipo = tipo;
            Console.Write("Informe o CEP: ");
            endereco.CEP = Console.ReadLine();
            Console.Write("Informe o Logradouro: ");
            endereco.Logradouro = Console.ReadLine();
            Console.Write("Informe o Numero: ");
            endereco.Numero = Console.ReadLine();
            Console.Write("Informe o Complemento: ");
            endereco.Complemento = Console.ReadLine();
            Console.Write("Informe o Bairro: ");
            endereco.Bairro = Console.ReadLine();
            Console.Write("Informe o Cidade: ");
            endereco.Cidade = Console.ReadLine();
            Console.Write("Informe o Estado: ");
            endereco.UF = Console.ReadLine();
            return endereco;
        }
    }
}
