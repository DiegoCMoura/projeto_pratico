﻿namespace Cadastro
{
    public class Contato
    {
        public TipoContato Tipo { get; set; }
        public string Dado { get; set; }

        public override string ToString() {
            switch (Tipo) {
                case TipoContato.Fixo: return $"Fixo: {Dado:(99) 9999-9999}";
                case TipoContato.Celular: return $"Celular: {Dado:(99) 99999-9999}";
                case TipoContato.Email: return $"Email: {Dado}";
                case TipoContato.Fax: return $"Fax: {Dado:(99) 9999-9999}";
                default: return string.Empty;
            }
        }
    }
}
