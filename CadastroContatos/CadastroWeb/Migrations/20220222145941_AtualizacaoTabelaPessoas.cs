﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CadastroWeb.Migrations
{
    public partial class AtualizacaoTabelaPessoas : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Contatos_Fisicas_PessoaFisicaId",
                table: "Contatos");

            migrationBuilder.DropForeignKey(
                name: "FK_Contatos_Juridicas_PessoaJuridicaId",
                table: "Contatos");

            migrationBuilder.DropForeignKey(
                name: "FK_Enderecos_Fisicas_PessoaFisicaId",
                table: "Enderecos");

            migrationBuilder.DropForeignKey(
                name: "FK_Enderecos_Juridicas_PessoaJuridicaId",
                table: "Enderecos");

            migrationBuilder.DropTable(
                name: "Fisicas");

            migrationBuilder.DropIndex(
                name: "IX_Enderecos_PessoaFisicaId",
                table: "Enderecos");

            migrationBuilder.DropIndex(
                name: "IX_Contatos_PessoaFisicaId",
                table: "Contatos");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Juridicas",
                table: "Juridicas");

            migrationBuilder.DropColumn(
                name: "PessoaFisicaId",
                table: "Enderecos");

            migrationBuilder.DropColumn(
                name: "PessoaFisicaId",
                table: "Contatos");

            migrationBuilder.RenameTable(
                name: "Juridicas",
                newName: "Pessoas");

            migrationBuilder.RenameColumn(
                name: "PessoaJuridicaId",
                table: "Enderecos",
                newName: "PessoaId");

            migrationBuilder.RenameIndex(
                name: "IX_Enderecos_PessoaJuridicaId",
                table: "Enderecos",
                newName: "IX_Enderecos_PessoaId");

            migrationBuilder.RenameColumn(
                name: "PessoaJuridicaId",
                table: "Contatos",
                newName: "PessoaId");

            migrationBuilder.RenameIndex(
                name: "IX_Contatos_PessoaJuridicaId",
                table: "Contatos",
                newName: "IX_Contatos_PessoaId");

            migrationBuilder.AlterColumn<string>(
                name: "RazaoSocial",
                table: "Pessoas",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AlterColumn<DateTime>(
                name: "DataAbertura",
                table: "Pessoas",
                type: "datetime2",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.AlterColumn<string>(
                name: "CNPJ",
                table: "Pessoas",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AddColumn<string>(
                name: "CPF",
                table: "Pessoas",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DataNascimento",
                table: "Pessoas",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                table: "Pessoas",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "RG",
                table: "Pessoas",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Pessoas",
                table: "Pessoas",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Contatos_Pessoas_PessoaId",
                table: "Contatos",
                column: "PessoaId",
                principalTable: "Pessoas",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Enderecos_Pessoas_PessoaId",
                table: "Enderecos",
                column: "PessoaId",
                principalTable: "Pessoas",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Contatos_Pessoas_PessoaId",
                table: "Contatos");

            migrationBuilder.DropForeignKey(
                name: "FK_Enderecos_Pessoas_PessoaId",
                table: "Enderecos");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Pessoas",
                table: "Pessoas");

            migrationBuilder.DropColumn(
                name: "CPF",
                table: "Pessoas");

            migrationBuilder.DropColumn(
                name: "DataNascimento",
                table: "Pessoas");

            migrationBuilder.DropColumn(
                name: "Discriminator",
                table: "Pessoas");

            migrationBuilder.DropColumn(
                name: "RG",
                table: "Pessoas");

            migrationBuilder.RenameTable(
                name: "Pessoas",
                newName: "Juridicas");

            migrationBuilder.RenameColumn(
                name: "PessoaId",
                table: "Enderecos",
                newName: "PessoaJuridicaId");

            migrationBuilder.RenameIndex(
                name: "IX_Enderecos_PessoaId",
                table: "Enderecos",
                newName: "IX_Enderecos_PessoaJuridicaId");

            migrationBuilder.RenameColumn(
                name: "PessoaId",
                table: "Contatos",
                newName: "PessoaJuridicaId");

            migrationBuilder.RenameIndex(
                name: "IX_Contatos_PessoaId",
                table: "Contatos",
                newName: "IX_Contatos_PessoaJuridicaId");

            migrationBuilder.AddColumn<int>(
                name: "PessoaFisicaId",
                table: "Enderecos",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PessoaFisicaId",
                table: "Contatos",
                type: "int",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "RazaoSocial",
                table: "Juridicas",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "DataAbertura",
                table: "Juridicas",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CNPJ",
                table: "Juridicas",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Juridicas",
                table: "Juridicas",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "Fisicas",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CPF = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    DataNascimento = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Nome = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    RG = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Fisicas", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Enderecos_PessoaFisicaId",
                table: "Enderecos",
                column: "PessoaFisicaId");

            migrationBuilder.CreateIndex(
                name: "IX_Contatos_PessoaFisicaId",
                table: "Contatos",
                column: "PessoaFisicaId");

            migrationBuilder.AddForeignKey(
                name: "FK_Contatos_Fisicas_PessoaFisicaId",
                table: "Contatos",
                column: "PessoaFisicaId",
                principalTable: "Fisicas",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Contatos_Juridicas_PessoaJuridicaId",
                table: "Contatos",
                column: "PessoaJuridicaId",
                principalTable: "Juridicas",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Enderecos_Fisicas_PessoaFisicaId",
                table: "Enderecos",
                column: "PessoaFisicaId",
                principalTable: "Fisicas",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Enderecos_Juridicas_PessoaJuridicaId",
                table: "Enderecos",
                column: "PessoaJuridicaId",
                principalTable: "Juridicas",
                principalColumn: "Id");
        }
    }
}
