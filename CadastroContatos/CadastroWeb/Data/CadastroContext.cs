﻿using CadastroWeb.Models;
using Microsoft.EntityFrameworkCore;

namespace CadastroWeb.Data
{
    public class CadastroContext: DbContext
    {
        public DbSet<Pessoa> Pessoas { get; set; }
        public DbSet<PessoaFisica> Fisicas { get; set; }
        public DbSet<PessoaJuridica> Juridicas { get; set; }

        public DbSet<Contato> Contatos { get; set; }
        public DbSet<Endereco> Enderecos { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            const string stConn = "Data source=(localdb)\\mssqllocaldb; Initial Catalog= Cadastro; Integrated Security=true; pooling=true";
            optionsBuilder
                .UseSqlServer(stConn)
                .LogTo(Console.WriteLine, LogLevel.Information)
                .EnableSensitiveDataLogging();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            foreach (var property in modelBuilder.Model.GetEntityTypes()
                .SelectMany(e => e.GetProperties()
                            .Where(p => p.ClrType == typeof(DateTime) &&
                                        p.GetColumnType() == null))) {
                property.SetColumnType("date");
            }
            foreach (var property in modelBuilder.Model.GetEntityTypes()
                .SelectMany(e => e.GetProperties()
                            .Where(p => p.ClrType == typeof(string) &&
                                        p.GetColumnType() == null))) {
                property.SetColumnType("varchar(100)");
            }

            modelBuilder.ApplyConfigurationsFromAssembly(typeof(CadastroContext).Assembly);

            modelBuilder
                .Entity<PessoaFisica>()
                .HasIndex(p => p.CPF)
                .HasDatabaseName("idx_pessoa_cpf")
                .HasFilter("CPF IS NOT NULL")
                .IsUnique();

            modelBuilder
                .Entity<PessoaJuridica>()
                .HasIndex(p => p.CNPJ)
                .HasDatabaseName("idx_pessoa_cnpj")
                .HasFilter("CNPJ IS NOT NULL")
                .IsUnique();

            base.OnModelCreating(modelBuilder);
        }
    }
}
