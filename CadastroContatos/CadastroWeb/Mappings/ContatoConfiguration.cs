﻿using CadastroWeb.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CadastroWeb.Mappings
{
    public class ContatoConfiguration : IEntityTypeConfiguration<Contato>
    {
        public void Configure(EntityTypeBuilder<Contato> builder)
        {
            builder
                .Property(p => p.Tipo)
                .HasConversion(new ConversoresValores.ConversorTipoContato());

            builder
                .Property<DateTime>("DataCriacao");
            builder
                .Property<DateTime>("DataAtualizacao");
        }
    }
}
