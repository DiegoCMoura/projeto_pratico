﻿using CadastroWeb.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CadastroWeb.Mappings
{
    public class PessoasConfiguration : IEntityTypeConfiguration<Pessoa>
    {
        public void Configure(EntityTypeBuilder<Pessoa> builder)
        {
            builder.ToTable("Pessoas");

            builder
                .HasDiscriminator<string>("TipoPessoa")
                .HasValue<PessoaFisica>("PF")
                .HasValue<PessoaJuridica>("PJ");

            builder
                .Property<DateTime>("DataCriacao");
            builder
                .Property<DateTime>("DataAtualizacao");
        }
    }
}
