﻿using CadastroWeb.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CadastroWeb.Mappings
{
    public class EnderecoConfiguration : IEntityTypeConfiguration<Endereco>
    {
        public void Configure(EntityTypeBuilder<Endereco> builder)
        {
            builder
                .Property(p => p.UF)
                .HasColumnType("varchar")
                .HasMaxLength(2);

            builder
                .Property(p => p.Tipo)
                .HasConversion(new ConversoresValores.ConversorTipoEndereco());

            builder
                .Property<DateTime>("DataCriacao");
            builder
                .Property<DateTime>("DataAtualizacao");
        }
    }
}
