﻿namespace CadastroWeb.Models
{
    public class PessoaFisica : Pessoa
    {
        public PessoaFisica() {
            Contatos = new List<Contato>();
            Enderecos = new List<Endereco>();
        }

        public string CPF { get; set; }
        public string RG { get; set; }

        public DateTime DataNascimento { get; set; }
    }
}
