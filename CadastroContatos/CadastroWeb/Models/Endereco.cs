﻿using CadastroWeb.ValueObjects.Enums;
using System.Text;

namespace CadastroWeb.Models
{
    public class Endereco
    {
        public int Id { get; set; }
        public TipoEndereco Tipo { get; set; }
        public string CEP { get; set; }
        public string Logradouro { get; set; }
        public string Numero { get; set; }
        public string Complemento { get; set; }
        public string Bairro { get; set; }
        public string Cidade { get; set; }
        public string UF { get; set; }

        public override string ToString()
        {
            var stringBuilder = new StringBuilder();

            switch (Tipo) {
                case TipoEndereco.Cobranca:
                    stringBuilder.AppendLine("Endereço Cobrança:");
                    return TextoEndereco(stringBuilder);
                case TipoEndereco.Entrega:
                    stringBuilder.AppendLine("Endereço Entrega:");
                    return TextoEndereco(stringBuilder);
                case TipoEndereco.Comercial:
                    stringBuilder.AppendLine("Endereço Comercial:");
                    return TextoEndereco(stringBuilder);
                default: return string.Empty;
            }
        }

        private string TextoEndereco(StringBuilder stringBuilder) {
            stringBuilder.AppendLine($"\tCEP: {CEP:99.999-999}");
            stringBuilder.AppendLine($"\tLogradouro: {Logradouro}");
            stringBuilder.AppendLine($"\tNumero: {Numero}");
            stringBuilder.AppendLine($"\tComplemento: {Complemento}");
            stringBuilder.AppendLine($"\tBairro: {Bairro}");
            stringBuilder.AppendLine($"\tCidade: {Cidade}");
            stringBuilder.AppendLine($"\tUF: {UF}");
            return stringBuilder.ToString();
        }
    }
}
