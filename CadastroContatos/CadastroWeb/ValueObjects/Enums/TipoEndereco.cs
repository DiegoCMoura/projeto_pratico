﻿namespace CadastroWeb.ValueObjects.Enums
{
    public enum TipoEndereco
    {
        Nenhum,
        Entrega,
        Cobranca,
        Comercial
    }
}
