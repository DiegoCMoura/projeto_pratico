﻿namespace CadastroWeb.ValueObjects.Enums
{
    public enum TipoContato
    {
        Email,
        Fixo,
        Celular,
        Fax,
        Beep
    }
}
