﻿namespace CadastroWeb.ValueObjects.Enums
{
    public enum TipoPessoa
    {
        Fisica,
        Juridica
    }
}
