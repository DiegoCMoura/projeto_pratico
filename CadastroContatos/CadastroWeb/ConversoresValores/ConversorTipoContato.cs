﻿using CadastroWeb.ValueObjects.Enums;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace CadastroWeb.ConversoresValores
{
    public class ConversorTipoContato : ValueConverter<TipoContato, string>
    {
        public ConversorTipoContato(): base(
            p => ConverterParaBanco(p),
            value => ConverterParaAplicacao(value),
            new ConverterMappingHints(3, unicode: false)) {}

        static string ConverterParaBanco(TipoContato tipo)
        {
            return tipo.ToString()[0..3].ToUpper();
            //return tipo.ToString().Substring(0, 3);
        }

        static TipoContato ConverterParaAplicacao(string value)
        {
            var tipo = Enum
                .GetValues<TipoContato>()
                .FirstOrDefault(p => p.ToString()[0..3] == value);

            return tipo;
        }
    }
}
