﻿using CadastroWeb.ValueObjects.Enums;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace CadastroWeb.ConversoresValores
{
    public class ConversorTipoEndereco : ValueConverter<TipoEndereco, string>
    {
        public ConversorTipoEndereco() : base(
                        p => ConverterParaBanco(p),
                        value => ConverterParaAplicacao(value),
                        new ConverterMappingHints(3, unicode: false)) { }

        static string ConverterParaBanco(TipoEndereco tipo)
        {
            return tipo.ToString()[0..3].ToUpper();
            //return tipo.ToString().Substring(0, 3);
        }

        static TipoEndereco ConverterParaAplicacao(string value)
        {
            var tipo = Enum
                .GetValues<TipoEndereco>()
                .FirstOrDefault(p => p.ToString()[0..3] == value);

            return tipo;
        }
    }
}
